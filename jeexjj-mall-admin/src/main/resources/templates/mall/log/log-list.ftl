<#--
/****************************************************
 * Description: t_mall_log的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>name</th>
	        <th>type</th>
	        <th>url</th>
	        <th>request_type</th>
	        <th>request_param</th>
	        <th>user</th>
	        <th>ip</th>
	        <th>ip_info</th>
	        <th>time</th>
	        <th>create_date</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.name}
			</td>
			<td>
			    ${item.type}
			</td>
			<td>
			    ${item.url}
			</td>
			<td>
			    ${item.requestType}
			</td>
			<td>
			    ${item.requestParam}
			</td>
			<td>
			    ${item.user}
			</td>
			<td>
			    ${item.ip}
			</td>
			<td>
			    ${item.ipInfo}
			</td>
			<td>
			    ${item.time}
			</td>
			<td>
			    ${item.createDate?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/log/input/${item.id}','修改t_mall_log','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/log/delete/${item.id}','删除t_mall_log？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>