package com.xjj.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xjj.framework.web.SpringControllerSupport;

/**
 * 访问根据目录直接跳转到登陆页，根目录为了显示商城前台，这里不再生效
 * @author zhanghejie
 */
@Controller
@RequestMapping("/") 
public class IndexController extends SpringControllerSupport{

	@RequestMapping(value = "/")
	public String index() {
		return "redirect:/passport/manager/login";
		//return null;
	}
	
	
}
