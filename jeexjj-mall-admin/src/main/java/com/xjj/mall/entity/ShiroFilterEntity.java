/****************************************************
 * Description: Entity for t_mall_shiro_filter
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ShiroFilterEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public ShiroFilterEntity(){}
    private String name;//name
    private String perms;//perms
    private Integer sortOrder;//sort_order
    /**
     * 返回name
     * @return name
     */
    public String getName() {
        return name;
    }
    
    /**
     * 设置name
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * 返回perms
     * @return perms
     */
    public String getPerms() {
        return perms;
    }
    
    /**
     * 设置perms
     * @param perms perms
     */
    public void setPerms(String perms) {
        this.perms = perms;
    }
    
    /**
     * 返回sort_order
     * @return sort_order
     */
    public Integer getSortOrder() {
        return sortOrder;
    }
    
    /**
     * 设置sort_order
     * @param sortOrder sort_order
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.ShiroFilterEntity").append("ID="+this.getId()).toString();
    }
}

