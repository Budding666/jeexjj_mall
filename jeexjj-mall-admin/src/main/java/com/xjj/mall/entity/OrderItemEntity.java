/****************************************************
 * Description: Entity for t_mall_order_item
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import java.math.BigDecimal;
import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class OrderItemEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public OrderItemEntity(){}
    private String itemId;//商品id
    private String orderId;//订单id
    private Integer num;//商品购买数量
    private String title;//商品标题
    private BigDecimal price;//商品单价
    private BigDecimal totalFee;//商品总金额
    private String picPath;//商品图片地址
    /**
     * 返回商品id
     * @return 商品id
     */
    public String getItemId() {
        return itemId;
    }
    
    /**
     * 设置商品id
     * @param itemId 商品id
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
    
    /**
     * 返回订单id
     * @return 订单id
     */
    public String getOrderId() {
        return orderId;
    }
    
    /**
     * 设置订单id
     * @param orderId 订单id
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    /**
     * 返回商品购买数量
     * @return 商品购买数量
     */
    public Integer getNum() {
        return num;
    }
    
    /**
     * 设置商品购买数量
     * @param num 商品购买数量
     */
    public void setNum(Integer num) {
        this.num = num;
    }
    
    /**
     * 返回商品标题
     * @return 商品标题
     */
    public String getTitle() {
        return title;
    }
    
    /**
     * 设置商品标题
     * @param title 商品标题
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    /**
     * 返回商品单价
     * @return 商品单价
     */
    public BigDecimal getPrice() {
        return price;
    }
    
    /**
     * 设置商品单价
     * @param price 商品单价
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    
    /**
     * 返回商品总金额
     * @return 商品总金额
     */
    public BigDecimal getTotalFee() {
        return totalFee;
    }
    
    /**
     * 设置商品总金额
     * @param totalFee 商品总金额
     */
    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }
    
    /**
     * 返回商品图片地址
     * @return 商品图片地址
     */
    public String getPicPath() {
        return picPath;
    }
    
    /**
     * 设置商品图片地址
     * @param picPath 商品图片地址
     */
    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.OrderItemEntity").append("ID="+this.getId()).toString();
    }
}

